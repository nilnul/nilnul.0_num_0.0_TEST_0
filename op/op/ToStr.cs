﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert1 =Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace nilnul.num.natural._test.op
{
	[TestClass]
	public class ToStr
	{
		[TestMethod]
		public void Do()
		{
			var d0 = natural.op.ToStrX.Do_decimal(0);
			var d1 = natural.op.ToStrX.Do_decimal(1);
			var d2 = natural.op.ToStrX.Do_decimal(2);
			var d9 = natural.op.ToStrX.Do_decimal(9);
			var d10 = natural.op.ToStrX.Do_decimal(10);
			var d11 = natural.op.ToStrX.Do_decimal(11);
			var d19 = natural.op.ToStrX.Do_decimal(19);


			Assert1.AreEqual(natural.op.ToStrX.Do_binary(0), "0");
			Assert1.AreEqual(natural.op.ToStrX.Do_binary(1), "1");
			Assert1.AreEqual(natural.op.ToStrX.Do_binary(2), "10");
			Assert1.AreEqual(natural.op.ToStrX.Do_binary(3), "11");


			Assert1.AreEqual(natural.op.ToStrX.Do_binary(4), "100");
			Assert1.AreEqual(natural.op.ToStrX.Do_binary(5), "101");

			Assert1.AreEqual(natural.op.ToStrX.Do_hex(0), "0");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(1), "1");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(2), "2");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(9), "9");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(10), "A");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(11), "B");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(15), "F");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(16), "10");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(256), "100");
			Assert1.AreEqual(natural.op.ToStrX.Do_hex(257), "101");


		}
	}
}
