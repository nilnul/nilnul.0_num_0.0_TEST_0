﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using A =Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace nilnul.num.natural._test.op
{
	[TestClass]
	public class ToStr
	{
		[TestMethod]
		public void Do()
		{
			var d0 = natural.op.ToStrX.Do_decimal(0);
			var d1 = natural.op.ToStrX.Do_decimal(1);
			var d2 = natural.op.ToStrX.Do_decimal(2);
			var d9 = natural.op.ToStrX.Do_decimal(9);
			var d10 = natural.op.ToStrX.Do_decimal(10);
			var d11 = natural.op.ToStrX.Do_decimal(11);
			var d19 = natural.op.ToStrX.Do_decimal(19);


			A.AreEqual(natural.op.ToStrX.Do_binary(0), "0");
			A.AreEqual(natural.op.ToStrX.Do_binary(1), "1");
			A.AreEqual(natural.op.ToStrX.Do_binary(2), "10");
			A.AreEqual(natural.op.ToStrX.Do_binary(3), "11");


			A.AreEqual(natural.op.ToStrX.Do_binary(4), "100");
			A.AreEqual(natural.op.ToStrX.Do_binary(5), "101");

			A.AreEqual(natural.op.ToStrX.Do_hex(0), "0");
			A.AreEqual(natural.op.ToStrX.Do_hex(1), "1");
			A.AreEqual(natural.op.ToStrX.Do_hex(2), "2");
			A.AreEqual(natural.op.ToStrX.Do_hex(9), "9");
			A.AreEqual(natural.op.ToStrX.Do_hex(10), "A");
			A.AreEqual(natural.op.ToStrX.Do_hex(11), "B");
			A.AreEqual(natural.op.ToStrX.Do_hex(15), "F");
			A.AreEqual(natural.op.ToStrX.Do_hex(16), "10");
			A.AreEqual(natural.op.ToStrX.Do_hex(256), "100");
			A.AreEqual(natural.op.ToStrX.Do_hex(257), "101");


		}
	}
}
