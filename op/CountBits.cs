﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using A = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace nilnul.num.natural._test.op
{
	[TestClass]
	public class CountBits
	{
		[TestMethod]
		public void Hyper_Eval_0pow()
		{
			_hyper_eval(0, 0, 0, 1);
			_hyper_eval(0, 0, 1, 0);
			_hyper_eval(0, 0, 2, 0);
			_hyper_eval(0, 0, 3, 0);
			_hyper_eval(0, 1, 0, 1);
			_hyper_eval(0, 1, 1, 1);
			_hyper_eval(0, 1, 2, 1);
			_hyper_eval(0, 1, 3, 1);
			_hyper_eval(0, 2, 0, 1);
			_hyper_eval(0, 2,1, 2);
			_hyper_eval(0, 2,2, 4);
			_hyper_eval(0, 2,3, 8);

			_hyper_eval(0, 3, 0, 1);
			_hyper_eval(0, 3, 1, 3);
			
			_hyper_eval(0, 3, 2, 9);
			_hyper_eval(1, 0, 0, 1);
			_hyper_eval(1, 0, 1, 0);

			_hyper_eval(1, 0, 2, 1);
			_hyper_eval(1, 0, 3, 0);
			_hyper_eval(1, 0, 4, 1);

			_hyper_eval(1, 1, 0, 1);
			_hyper_eval(1, 1, 1, 1);
			_hyper_eval(1, 1, 2, 1);
			_hyper_eval(1, 1, 3, 1);
			_hyper_eval(1, 2, 0, 1);
			_hyper_eval(1, 2, 1, 2);
			_hyper_eval(1, 2, 2, 4);
			_hyper_eval(1, 2, 3, 16);
			_hyper_eval(1, 2, 4, 65536);

			_hyper_eval(1, 3, 0, 1);
			_hyper_eval(1, 3, 1, 3);
			_hyper_eval(1, 3, 2, 27);
			_hyper_eval(1, 3, 3, 7625597484987);

			_hyper_eval(2, 3, 2, 7625597484987);

		}

		private void _hyper_eval(int order,int base_,int index, long result) {

			A.AreEqual(
				nilnul.num.op.Hyper.Eval_0multi(order+1,base_,index),result
			);


		}
	}
}
