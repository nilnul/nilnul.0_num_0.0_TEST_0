﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.num
{
	public class Eq : IEqualityComparer<nilnul.Num>, IEqualityComparer<nilnul.NumI>
	{
		public bool Equals(Num x, Num y)
		{
			return x.val == y.val;
			throw new NotImplementedException();
		}

		public int GetHashCode(Num obj)
		{
			return obj.val.GetHashCode();
			throw new NotImplementedException();
		}

		public bool Equals(NumI x, NumI y)
		{
			return x.toBigint() == y.toBigint();
			throw new NotImplementedException();
		}

		public int GetHashCode(NumI obj)
		{
			return obj.toBigint().GetHashCode();
			throw new NotImplementedException();
		}

		static public readonly Eq Singleton = nilnul.obj.SingletonByDefault<Eq>.Instance;

	}
}
