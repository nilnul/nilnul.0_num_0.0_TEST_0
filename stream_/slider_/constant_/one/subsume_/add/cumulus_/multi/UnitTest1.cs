﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.stream_.slider_.constant_.one.subsume_.add.cumulus_.multi
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var factorial = new nilnul.num.stream_.slider_.constant_.one.subsume_.add.cumulus_.Multi();

			var r = new[] {1,1,2,6,24,120,720 };

			for (int i = 0; i < r.Length; i++)
			{
				Debug.Assert( factorial.next() == r[i]);
			}

		}
	}
}
