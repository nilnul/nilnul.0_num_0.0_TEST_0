using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.stream_.slider_.factorial
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var x = new num.stream_.slider_.Factorial();

			var fetched = nilnul.obj.stream._FetchX.Fetch(x, 10).Select(x=>(int)(x.en)).ToArray();

			var re=new nilnul.obj.str.re_.Unforked<int>().re(
				fetched
				,
				new[] { 1,1,2,6,24,120,720, }
			);
			Debug.WriteLine(
				nilnul.obj.str.PhraseX.Phrase(fetched)
			);

			Assert.IsTrue(re);
		}
	}
}
