using System;
using System.Collections;
using System.Numerics;
using nilnul._num.bigint.be;

namespace nilnul.num.op_
{
	

	static public  class _FactorialX
	{


		static public BigInteger _Eval_byLoop(BigInteger _natural)
		{


			BigInteger r = 1;
			for (BigInteger i =_natural; i >1;i-- )
			{
				r *= i;
			}
			return r;
		}

	
		static public BigInteger _Eval_recur(this BigInteger _natural)
		{
			if (_natural == 0) return 1;

			return _natural * _Eval_recur(_natural-1);

		}

		
	
		
	}

	public class Factorial : OpI
	{
		public Natural.Asserted eval(Natural.Asserted arg)
		{
			return new Natural.Asserted(
					_FactorialX._Eval_byLoop(arg.val)
				);
			throw new NotImplementedException();
		}

		static public readonly Factorial Singleton = SingletonByDefault<Factorial>.Instance;






	}



}
