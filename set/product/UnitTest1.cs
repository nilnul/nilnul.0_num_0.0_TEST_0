using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul.num.integer.set;
using System;

namespace nilnul.num.set.product
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			var t = nilnul.num.integer.bound_._Closed1nX._Seq_1tilLtMax(1, 6);

			var t1 = nilnul.num.integer.bound_._Closed1nX._Seq_1tilLtMax(8, 10);

			Assert.AreEqual(

				t.Product()
				,
				t1.Product()
			);
		}
	}
}
