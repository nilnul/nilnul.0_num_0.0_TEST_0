﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.num.bigint.be;
using N = nilnul.NumI;
using System.Numerics;

namespace nilnul.num
{
	public class Comparer2 : IComparer<N>
	{


		

		public int Compare(N x, N y)
		{
			return BigInteger.Compare(x.toBigint(), y.toBigint());
		}


		static public Comparer2 Singleton
		{
			get
			{
				return nilnul.obj.SingletonByDefault<Comparer2>.Instance;
			}
		}




	}
}
