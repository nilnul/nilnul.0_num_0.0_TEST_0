﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using A= Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace nilnul.num.natural._test.seq
{
	[TestClass]
	public class FibonacciTest
	{
		[TestMethod]
		public void Fibonacci_item_byRecurWithFold()
		{
			var list = new List<int>();
			for (int i = 0; i < 10; i++)
			{
				list.Add(
					natural.sequence.Fibonacci.Item_tailRecurByFold(i)
					);
				
			}

			
		}
		[TestMethod]
		public void Fibonacci_Item()
		{
			uint result = 13;

			var item = nilnul.num.natural.sequence.Fibonacci.Item2(7);

			A.AreEqual(item, (ulong)13);
		}
	}
}
