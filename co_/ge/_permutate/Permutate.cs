﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using nilnul._num.bigint.be;
using nilnul.num.duo.op_._permutate;


using N = nilnul._num.bigint.be.Natural.Asserted;



namespace nilnul.num.duo_.ge._permutate
{
	static public class Extensions
	{
		
		
		public static BigInteger _Permutate(this BigInteger _total_natural, BigInteger _selected_natural_leTotal)
		{
			BigInteger r = 1;
			for (BigInteger i = _total_natural; i > _total_natural- _selected_natural_leTotal; i--)
			{
				r *= i;
			}
			return r;


		}

		static public N Permutate(N total, N selected) {

			return new N( _Permutate(total.val, selected.val));

		}
	}

}
