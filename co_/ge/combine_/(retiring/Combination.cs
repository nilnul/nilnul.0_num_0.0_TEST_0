﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul._num.bigint.be;
using nilnul.num.duo.op_._combi;

using N = nilnul._num.bigint.be.Natural.Asserted;
using System.Numerics;

namespace nilnul.num.duo.op_
{

	[Obsolete(nameof(duo_.ge.combine_._combinate.Extensions))]
	static public class _CombinateStatic
	{

		static public BigInteger _Eval(BigInteger n_natural, BigInteger k_leN) {
				return 
					
						nilnul.num.duo.op_._PermutateStatic._Permutate(
							n_natural,k_leN
						)
					/
					nilnul.num.op_._FactorialX._Eval_byLoop(
						k_leN	
					)
				
			
			;
		}

		static public N Eval(Be.En arg) {
				return new N(
					( nilnul.num.duo.op_._PermutateStatic._Permutate(
						arg.asserted.Item1
						, arg.asserted.Item2
					)
					/
					nilnul.num.op_._FactorialX._Eval_byLoop(
						arg.asserted.Item2	
					)
				)
			)
			;
		}

	}
	public class Combination
		:OpI
	{
		public  N eval(Be.En arg)
		{
			return nilnul.num.op_.Factorial.Singleton.eval(arg.asserted.Item1) / ( nilnul.num.op_.Factorial.Singleton.eval(arg.asserted.Item2)
				*nilnul.num.op_.Factorial.Singleton.eval(
					arg.asserted.Item1 -arg.asserted.Item2	
				)
				);
			//throw new NotImplementedException();
		}

		public N eval(N a, N b)
		{
			return eval(
				new Be.En(a, b)
			);
			//throw new NotImplementedException();
		}
	}
}
