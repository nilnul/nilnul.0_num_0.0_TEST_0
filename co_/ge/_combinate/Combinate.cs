﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

using N = nilnul._num.bigint.be.Natural.Asserted;


namespace nilnul.num.duo_.ge.combine_._combinate
{
	public class Extensions
	{

		static public BigInteger _Eval(BigInteger n_natural, BigInteger k_leN)
		{
			return

					nilnul.num.duo.op_._PermutateStatic._Permutate(
						n_natural, k_leN
					)
				/
				nilnul.num.op_._FactorialX._Eval_byLoop(
					k_leN
				)
		;
		}

		static public N _Eval(N total, N selected) {
			return new N(
				_Eval(total.val, selected.val)
			);
		}


	}
}
