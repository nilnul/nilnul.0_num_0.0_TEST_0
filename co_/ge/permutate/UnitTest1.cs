﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.co_.ge.permutate
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNum(0) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNum(1) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNum(2) ==2

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNum(3) ==6

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNum(10) ==3628800

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(0,0) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(1,0) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(1,1) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(2,0) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(2,1) ==2

			);
			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(2,2) ==2

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(3,0) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(3,1) ==3

			);
			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(3,2) ==3*2

			);
			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(3,3) ==3*2*1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(10,0) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(10,1) ==10

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(10,2) ==90

			);

			Debug.Assert(
				nilnul.num.co_.ge._PermutateX._assumeNumGe(10,10) ==3628800

			);


		}


	}
}
