﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.co_.ge.combinate
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(0, 0) == 1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(1, 0) == 1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(1, 1) == 1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(2, 0) == 1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(2, 1) == 2

			);
			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(2, 2) ==1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(3, 0) == 1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(3, 1) == 3

			);
			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(3, 2) == 3 

			);
			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(3, 3) ==  1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(10, 0) == 1

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(10, 1) == 10

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(10, 2) == 45

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(10, 5) == 10*9*8*7*6/5/4/3/2

			);

			Debug.Assert(
				nilnul.num.co_.ge._CombinateX._assumeNumGe(10, 10) == 1

			);
		}
	}
}
