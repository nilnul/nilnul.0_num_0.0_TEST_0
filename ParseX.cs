﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Diagnostics;
using Assert1 = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;


namespace nilnul.num.natural._test
{
	[TestClass]
	public class ParseX
	{
		[TestMethod]
		public void parseX_eval()
		{

			try
			{		
				var _null = _eval_byCase(null);

				throw new Exception();


			}
			catch (ArgumentNullException)
			{
				
				
			}
			catch(NullReferenceException){}


			try
			{
				var _invalid = _eval_byCase("12");

				throw new Exception();


			}
			catch (ArgumentException)
			{


			}

			try
			{
				var _invalid = _eval_byCase("1 0");

				throw new Exception();


			}
			catch (ArgumentException)
			{


			}



			var _empty = _eval_byCase("");
			var _whitespace = _eval_byCase("  ");

			var _0 = _eval_byCase("0");
			var _000 = _eval_byCase("00");
			var _01 = _eval_byCase("01");

			var _010 = _eval_byCase("010");
			var _1 = _eval_byCase("1");

			var _10 = _eval_byCase("10");

			Assert.IsTrue(_eval_byCase("1100") == 12);

			Debug.WriteLine(_eval_byCase("01011010"));
			


		}

		private BigInteger _eval_byCase(string x) {
			return nilnul.num.natural.ParseX.EvalFroBinary(x);
		}


	}
}
