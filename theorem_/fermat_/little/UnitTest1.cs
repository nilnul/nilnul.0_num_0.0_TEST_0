﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace nilnul._num_._TEST_.theorem_.fermat_.little
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var primes =  nilnul.num_.plural_.prime.sortie_.db_.ef_.AutoGrowDb.SequenceUpTo_inLong(100).ToArray();

			foreach (var item in primes)
			{
				t(item);
			}
		}

		void t(long p)
		{
			for (int i = 0; i < 100; i++)
			{
				nilnul.num._theorem.conjecture_.FermatLittle._Vow_0nat_1prime(
					i,
					p
				);

			}
		}
	}
}
