﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.num.ext
{
	public class Comparer1 : IComparer<ExtA1>
	{
		public static readonly Comparer1 Instance = new Comparer1();
		public int Compare(ExtA1 x, ExtA1 y)
		{
			if (x is Inf1)
			{
				if (y is Inf1)
				{
					return 0;
				}
				return 1;
			}
			if (y is Inf1)
			{
				return -Compare(y, x);
			}
			return nilnul.num.Comparer1.Instance.Compare((x as Basic ).val, (y as Basic).val);

			throw new NotImplementedException();
		}
	}
}
