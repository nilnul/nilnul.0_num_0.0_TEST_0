﻿using System;
using System.Diagnostics;
using System.Numerics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.op_.ternary_.knuth
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			///tetrate(2,x). x is the par below. and the last par is the expected result.
			vowEq(0,0,0, 0);
			vowEq(0,0,1,  0);
			vowEq(0,0,2, 0);

			vowEq(0,0,3, 0);


			vowEq(0,1,0, 0)
				;

			vowEq(0,1,1, 1)
				;

			vowEq(0,1,2, 2)
				;
			vowEq(0,1,3, 3)
				;
			vowEq(0, 2, 0, 0);
			vowEq(1, 0, 0, 1);
			vowEq(1, 0, 1, 0);
			vowEq(1, 0, 2, 0);
			vowEq(1, 0, 3, 0);
			vowEq(1, 1, 0, 1);
			vowEq(1, 1, 1, 1);
			vowEq(1, 3, 1, 3);

			vowEq(2, 0,0 , 1);
			vowEq(2, 0,1 , 0);
			vowEq(2, 0, 2, 1);
			vowEq(2, 1, 0, 1);
			vowEq(2, 2, 0, 1);

			vowEq(2, 3, 1, 3);
			vowEq(2, 3, 2, 27);
			vowEq(2, 3, 3, BigInteger.Pow(3,27));

			vowEq(2, 1,1 , 1);
			vowEq(2, 1,2 , 1);

			vowEq(2, 2,0 , 1);
			vowEq(2, 2,1 , 2);
			vowEq(2, 2,2 , 4);
			vowEq(2, 2,3 , 16);
			//vowEq(4, 2,4 , 65536);

			//vowEq(4, 2,5 , BigInteger.Pow(2,65536));


			vowEq(3, 0, 0, 1);

			vowEq(3, 0, 1, 0);
			vowEq(3, 1, 0, 1);
			vowEq(3, 1, 2, 1);

			vowEq(3, 2, 2, 4);




		
			
		}

		void vowEq(BigInteger ord,BigInteger basis,BigInteger index, BigInteger expected) {

			Debug.Assert(
				nilnul.num.op_.ternary_._KnuthX._Op_assumeNats(ord, basis,index)
				==expected
			);
		}
	}
}
