﻿using System;
using System.Diagnostics;
using System.Numerics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.op_.binary_.tetrate
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			///tetrate(2,x). x is the par below. and the last par is the expected result.
			vowEq(0,1);
			vowEq(1, 2);
			vowEq(2, 4);
			vowEq(3, 16);
			vowEq(4, 65536)
				;
			vowEq(5,
				BigInteger.Pow(2, 65536)
			); ;

			
		}

		void vowEq(BigInteger x, BigInteger y) {

			Debug.Assert(
nilnul.num.op_.binary_.Tetrate.Op_assumeNats(2,x)
==y
			);
		}
	}
}
