﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using nilnul.obj.str;
using System.Diagnostics;

namespace nilnul._num_._TEST_.ask_.digitsSep
{
	/// <summary>
	/// use "+", "-" to connect:
	///		123456789
	///		like:
	///			123+456-78+9
	///			,
	///			etc
	///	such that the calculated is 100.
	/// </summary>
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			var s = "123456789";

			_seq_assumeDwelt(s).Where(
				t=> parse(t)==100
			).ForEach(
				X=>Trace.WriteLine(X.Replace(" ",""))
			);

		}

		IEnumerable<string> _seq_bodyAssumeDwelt(char c, string s) {
			

			foreach (var substr in _seq_assumeDwelt(s) )
			{
				yield return $"{c} {substr}";
				yield return $"{c}+{substr}";
				yield return $"{c}-{substr}";

			}

		}
		IEnumerable<string> _seq_assumeDwelt( string s) {
			var body = s.Substring(1);

			if (body.Any())
			{
				return _seq_bodyAssumeDwelt(s[0], body);
			}
			else
			{
				 return new[] { s };
			}

		}


		int parse(string s) {
			var split=Regex.Split(s, "[+]|(?=-)");

			return split.Select(c=>int.Parse(c.Replace(" ",""))).Sum();
			


		}
	}
}
