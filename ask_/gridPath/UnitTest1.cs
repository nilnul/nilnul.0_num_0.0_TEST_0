﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._num_._TEST_.ask_.gridPath
{
	/// <summary>
	/// a grid w*h, where w,h is positive nums.
	/// a path is from topleft to downright, moving only down or left one grid by one grid.
	/// how many paths total?
	/// </summary>
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

		}


		static public int AssumePositives(int w, int h) {
			if (w==1)
			{
				return 1;
			}
			if (h==1)
			{
				return 1;
			}

			return AssumePositives(w - 1, h) + AssumePositives(w, h - 1);
		}
	}
}
