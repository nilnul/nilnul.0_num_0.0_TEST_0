﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._num_._TEST_.ask_.partition_
{
	/// <summary>
	/// partition 1000 apples into 10 boxes, such that we can present a few boxes without breaking them, that have a given number in [0,1000).
	/// 
	/// </summary>
	class OneKilo
	{
	}
	/*ans:
	  1,2,4,8,16,32,64,128,256, 489.

	if you want 0, we give you no boxes.
	if you want 1, we give you the box of 1.
	if you want 2, we give you the box of 2
	if you want 3, we give you the boxes: 1,2
	...
	 
	 */
}
