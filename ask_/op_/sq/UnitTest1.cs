﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;

namespace nilnul._num_._TEST_.ask_.op_.sq
{
#if TRIAL
#endif

	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var stream = new nilnul.num.stream_.slider_.PositiveSquared();

			IEnumerable<char> chars()
			{
				while (true)
				{
					var current = stream.current;
					var txt = current.ToString();
					foreach (var item in txt)
					{
						yield return item;
					}
					stream.moveNext();
				}
			}

			var t = chars();

			char c= t.ElementAt(1010-1);

			Trace.WriteLine(c);
			Trace.Assert(c=='3');


		}
	}
}
