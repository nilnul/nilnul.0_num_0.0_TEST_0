﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._num_._TEST_.ask_.partitionIntoFiniteBoxes
{
	/// <summary>
	/// we have n objects, put them into boxes,each of which at most can hold m ones, and at least shall hold one. where n, m are natural nums.
	/// The boxes are all the same; this means, after partition, we collect all the boxes that has at least one. the count of what is inside box is put inside a bag, or multiset. How many multiset can we get? You can infer that the order is unimportant as we use a bag structure.
	/// 
	/// </summary>
	/// for such 2d problems, we can put in 2d trying to wind down the d1, and d2 in two directions.
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
		}

		static public int CountOfBags(uint n, uint m) {
			if (n==0)
			{
				return 1; // the bag is empty
			}
			/// we have at least one to partition
			if (m==0)
			{
				return 0;
			}
			if (n<m)
			{
				return CountOfBags(n, n);

			}

			return CountOfBags(
				n
				,
				m-1
			)
				+
				CountOfBags(
					n-m
					,
					m

			);


		}
	}
}
