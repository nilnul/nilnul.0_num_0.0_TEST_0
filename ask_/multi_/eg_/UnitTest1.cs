﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._num_._TEST_.ask_.multi_.eg_
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			/// 小华、小明分别从一座桥的两端同时出发，相向而行，两人首次在离小华出发点65米处相遇. 然后两人各自继续前行到达桥的另一端后，两人立即以原速原路返回，又在离小华出发点45米处相遇。这座桥长多少米？
			///
			/// ans:
			/// 两人速度不变，第一次相遇两人合计走过的是一个桥的全长，第二次相遇是两人合计走过的是三个桥的全长;
			/// 小华第一次相遇走了65米，那么第二次相遇走了195米; 小华再走45米就 单人走过了两个桥长。
			/// therefore (195 + 45 ) /2 =120 is the length of the bridge;
		}
	}
}
