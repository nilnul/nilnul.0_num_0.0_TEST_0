﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._num_._TEST_.nilnul0.algo_.loop.surround.ask_
{
	internal class FrogJumpOutWell
	{
		/// a well is deep at 40m.
		/// a frog climbs 3m in the daytime and fall back 1m in the night. How many days will it take to get out?
		///
		/// answer:
		///		let's first take the most rough calibration:
		///			40/3 = 13.
		///		then take a not so rough calibration:
		///			40/(3-1) =20.
		///		let's finetune the 20days.
		///			for 17days, the frog will be at 34m
		///			at 18days, 36m
		///			at 19days, 39m then 38m.
		///			at 20days, 41m. it will be out, let go of the night.
		///			so it's 19.5days;
		///


		/// a well is deep at 40m.
		/// a frog climbs 4m in the daytime and fall back 1m in the night. How many days will it take to get out?
		///
		///	analyze:
		///		just imagine that the well is infinitely deep.
		///		the question is: when will the frog past the point at 40m?
		///		
		/// answer:
		///		let's first take the most rough calibration:
		///			40/4 = 10.
		///		then take a not so rough calibration:
		///			40/(4-1) =13.
		///		let's finetune the 13days. as one day is at most 4 meters, let's backtrack 4meters from 13*3, that is 39-4 = 35m.
		///		35m/ 3= 11days. we know at the end of day11, the frog has never passed point at 40m.
		///		that is at day11, 33m.
		///		at day 12, 36m. (37m in daytime, 36m in night)
		///		at day 13, 40m in daytime, le got of the night.
		///		so in all it takes 12.5days.
		///

	}
}
