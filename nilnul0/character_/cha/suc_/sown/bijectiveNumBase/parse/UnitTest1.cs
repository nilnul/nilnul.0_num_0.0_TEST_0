﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul.character._test.suc_.sown.bijectiveNumBase.parse
{
	[TestClass]
	public class UnitTest1
	{
			static nilnul.character_.cha.sortie_.sown.bijectiveNumBase_.CapitalLetter capitalBase = nilnul.character_.cha.sortie_.sown.bijectiveNumBase_.CapitalLetter.Singleton;
		[TestMethod]
		public void TestMethod1()
		{

			compare(0, "");
			compare(1, "A");
			compare(2, "B");
			compare(26, "Z");
			compare(27, "AA");
			compare(28, "AB");
			compare(26*26 +26, "ZZ");
		}

		private void compare(int  x, string expected) {


			nilnul.obj.vow_.True.Singleton.vow(
				
				capitalBase.parse_bigEndian(expected)
				
				==x
			);
		}
	}
}
