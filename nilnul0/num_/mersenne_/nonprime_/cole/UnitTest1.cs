﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Numerics;

namespace nilnul._num_._TEST_.nilnul0.num_.mersenne_.nonprime_.cole
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			///composed of two prime numbers multiplied together.
			var r = BigInteger.Parse(
				@"147,573,952,589,676,412,927".Replace(",","")
			);


			Assert.IsTrue(
				r
				==
				BigInteger.Parse(
					"193_707_721".Replace("_","")
)
*
BigInteger.Parse(
	"761_838_257_287".Replace("_","")
)
			);
		}
	}
}
