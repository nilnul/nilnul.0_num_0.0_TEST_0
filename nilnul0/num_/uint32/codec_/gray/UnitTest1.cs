﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace nilnul._num_._TEST_.nilnul0.num_.uint32.codec_.gray
{
	[TestClass]
	public class UnitTest1
	{
		void v(int i)
		{
			var nilAsBits = nilnul.num_.uint32.codec_._GrayCodecX.Num2gray(
				BitConverter.ToUInt32(
					BitConverter.GetBytes(i)
					, 0
				)
			);

			var one8bits = nilnul.num_.uint32.codec_._GrayCodecX.Num2grayAsUint(
				i + 1
			);

			Assert.IsTrue(
				nilnul.bit.vec.re_._OneBitDifX.IsOneBitDif(nilAsBits, one8bits)
			);
		}
		void v4minus(uint i)
		{
			var nilAsBits = nilnul.num_.uint32.codec_._GrayCodecX.Num2gray(
				i
			);

			var one8bits = nilnul.num_.uint32.codec_._GrayCodecX.Num2gray(
				i - 1
			);

			Assert.IsTrue(
				nilnul.bit.vec.re_._OneBitDifX.IsOneBitDif(nilAsBits, one8bits)
			);
		}
		void v4plus(uint i)
		{
			var nilAsBits = nilnul.num_.uint32.codec_._GrayCodecX.Num2gray(
				i
			);

			var one8bits = nilnul.num_.uint32.codec_._GrayCodecX.Num2gray(
				unchecked(i + 1)
			);

			Assert.IsTrue(
				nilnul.bit.vec.re_._OneBitDifX.IsOneBitDif(nilAsBits, one8bits)
			);
		}



		[TestMethod]
		public void TestMethod1()
		{

			const int intial = 10000;

			for (int i = 0; i < intial; i++)
			{
				v(i);
			}


			for (uint i = uint.MaxValue; i > uint.MaxValue - intial; i--)
			{
				v4minus(i);

			};

			v4plus(uint.MaxValue);


			var rnd = new Random();

			var t = rnd.Next(intial, int.MaxValue - intial);


			unchecked
			{
				for (int i = t; i < t + intial; i++)
				{
					v(i);

				}

			}






		}

	}
}
