﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul.num_;
using System;

namespace nilnul._num_._TEST_.nilnul0.num_._radix.radic.ponented.phrase
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			v(
				0
				,2
				,
				"2^0"
			);
			v(
				0
				,7
				,
				"7^0"
			);
			v(
				0
				,10
				,
				"10^0"
			);
			v(
				0
				,16
				,
				"16^0"
			);
			v(
				1
				,2
				,
				"2^1"
			);
			v(
				1
				,7
				,
				"7^1"
			);
			v(
				1
				,10
				,
				"10^1"
			);
			v(
				1
				,16
				,
				"16^1"
			);
			v(
				2
				,2
				,
				"2^2"
			);

			v(
				2
				,7
				,
				"7^2"
			);
			v(
				2
				,10
				,
				"10^2"
			);
			v(
				2
				,16
				,
				"16^2"
			);


			v(
				3
				,2
				,
				"2^3"
			);
			v(
				3
				,3
				,
				"3^3"
			);

			v(
				3
				,10
				,
				"10^3"
			);

			v(
				30
				,10
				,
				"10^30"
			);


			v(
				4
				,2
				,
				"2^4"
			);
			v(
				4
				,3
				,
				"3^4"
			);
			v(
				4
				,10
				,
				"10^4"
			);
			v(
				4
				,16
				,
				"16^4"
			);

			v(
				7
				,16
				,
				"16^7"
			);
			v(
				10
				,10
				,
				"10^10"
			);

			v(
				15
				,10
				,
				"10^15"
			);

			v(
				15
				,16
				,
				"16^15"
			);

			v(
				16
				,10
				,
				"10^16"
			);
			v(
				16
				,16
				,
				"16^16"
			);



		}

		void v(int x, int radic, string expected) {
			v(
				nilnul.Num_ofIn.Of(x)
				,
				radic
				,
				expected
			);
		}

		private void v(Num_ofIn num_ofIn, int radic,string expected)
		{
			v(
				new nilnul.num_._radix.radic.Ponented(radic,num_ofIn)
				,
				expected
			);
		}

		private void v(nilnul.num_._radix.radic.Ponented radix2, string expected)
		{
			v(
				(radix2.ToString()),expected
			);
		}

		void v(string actual,string expected) {
			Assert.IsTrue(actual==expected);
		}
	}
}
