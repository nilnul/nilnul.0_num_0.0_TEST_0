﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Factor1 = nilnul.num.positive._factor.ToIncreasingStr;
using System.Diagnostics;
using Prime = nilnul.num.plural.be.Prime.Asserted;
using Positive = nilnul.num.bigint.be.Positive.Asserted;

namespace nilnul.num.positive._factor
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void ToIncreaseStr()
		{

			assert(1, 
				new prime.Bag() {

				}	
			);

			assert(2,
				 prime.Bag.Create(2,1

					 )
					
			);







			
			assert(3, 3, 1);


			assert(4, 2, 2);
			assert(5, 5, 1);
			assert(6, 2, 1, 3, 1);
			assert(7, 7, 1);
			assert(8, 2, 3);
			assert(9, 3, 2);
			assert(10, 2, 1, 5, 1);
			assert(11, 11, 1);
			assert(12, 3, 1, 2, 2);
			assert(13, 13, 1);
			assert(60, 2, 2,		3, 1,		 5, 1);
			assert(3600, 2, 4,		3, 2,		 5, 2);

			
		}

		private nilnul.num.prime.Bag _factor(int x) {

			return  /*Factor1*/nilnul.num.positive.Factor._Eval(x);

		}

		private void assert(int x, prime.Bag bag) {
			Debug.Assert(
				nilnul.num.prime.bag.Sube.Instance.eq(
					_factor(x)
					,
					bag	
				)	
			);
		}


		private void assert(int x, params int[] bag) {
			assert(x, prime.Bag.Create(bag));
		}



	}
}
