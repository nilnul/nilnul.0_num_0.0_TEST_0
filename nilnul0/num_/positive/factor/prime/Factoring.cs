﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace nilnul.num.natural._test.prime
{
	[TestClass]
	public class Factoring
	{
		[TestMethod]
		public void Factoring_test()
		{
			var r = /*nilnul.num.natural.prime.collection.AutoGrowMem*/nilnul.num_.plural_.prime.set_.db_.ef.AutoGrowMem.IsPrime(100);

			Debug.Assert(!r);

			var factored = nilnul.num.positive.Factor._Eval(100);
			var factoredOf1 = /*nilnul.num.natural.prime.Factoring.Eval*/nilnul.num.positive.Factor._Eval(1);

			Debug.Assert(factoredOf1.Count == 0);

			var factoredOf2 = /*nilnul.num.natural.prime.Factoring.Eval*/nilnul.num.positive.Factor._Eval(2);

			Debug.Assert(factoredOf2.Count == 1 && factoredOf2[new nilnul.num.plural.be.Prime.Asserted( 2 )] == 1);




		}


	}
}
