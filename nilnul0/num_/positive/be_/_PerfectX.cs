using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.num_.positive.be_
{
	internal class _PerfectX
	{
		static public bool _Be_0positive(int positive) {
			var factors = num_.positive.factors_._LtSelfX._Factors_0positive(positive);

			return factors.Sum() == positive;
		}
		static public bool _Be_0positive(long positive) {
			var factors = num_.positive.factors_._LtSelfX._Factors_0positive(positive);

			return factors.Sum() == positive;
		}


	}
}
