using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.num_.positive.theorem_._fermatLast
{
	internal class _EquationX
	{
		static public bool _Equation_0poly_123positive(BigInteger ponent,BigInteger x, BigInteger y, BigInteger z) {
			return nilnul.num.op_.binary_._PowX._Op_0nonneg_1nonneg(x, ponent)
				+ nilnul.num.op_.binary_._PowX._Op_0nonneg_1nonneg(y, ponent)
				==
				nilnul.num.op_.binary_._PowX._Op_0nonneg_1nonneg(z, ponent)
			;
		}

	}
}
