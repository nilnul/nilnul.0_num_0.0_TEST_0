﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Numerics;

namespace nilnul._num_._TEST_.nilnul0.num_.positive.of_.unary_.lb
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			v(1,0);
			v(2, 1);
			v(3, 1);
			v(4, 2);
			v(5, 2);
			v(6, 2)
			;
			v(7, 2);
			v(8, 3)
				;
			v(9, 3)
				;
			v(15, 3);
			v(16, 4)
				;
			v(17, 4);
			v(31, 4);
			v(32, 5);
			v(33, 5);
			v(63, 5);
			v(64, 6);
			v(65, 6);
			v(127, 6);
			v(128, 7);
			v(129, 7);
			v(255, 7);
			v(256, 8);
			v(257, 8);
			v(511, 8);
			v(65535, 15);
			v(65536, 16);
			v(65537, 16);

			v(65536*2-1, 16);
			v(65536*2, 17);
			v(65536*2+1, 17);
			v(65536*2+2, 17);

		}

		void v(BigInteger x,  BigInteger expected) {
			Assert.IsTrue(
				nilnul.num_.positive.to_._LbX.Lb(x)==expected
			);
		}
	}
}
