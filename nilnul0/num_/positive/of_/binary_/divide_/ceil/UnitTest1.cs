﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul._num_._TEST_.nilnul0.num_.positive.of_.binary_.divide_.ceil
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					1,1
				)==1

			);
			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					1,2
				)==1

			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					1,3
				)==1

			);
			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					1,4
				)==1

			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					2,1
				)==2

			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					2,2
				)==1

			);
			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					2,3
				)==1
			);
			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					2,4
				)==1
			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					2,45
				)==1
			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					3,1
				)==3
			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					3,2
				)==2
			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					3,3
				)==1
			);

			Assert.IsTrue(
				nilnul.num_.positive.of_.binary_.divide_._CeilingX.DivideCeilingAsPositive(
					3,4
				)==1
			);
		}
	}
}
