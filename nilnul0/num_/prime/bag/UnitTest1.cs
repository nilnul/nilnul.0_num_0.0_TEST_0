﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace nilnul.num_.plural_.prime.bag
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var bag = new nilnul.num_.plural_.prime.Bag(2,7,2);
			Debug.Assert(
				bag.multiplicity(new Prime( 2 ))==2
			);
		}
	}
}
