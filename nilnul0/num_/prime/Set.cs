﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul.num.natural.prime._test
{
	/// <summary>
	/// Summary description for UnitTest3
	/// </summary>
	[TestClass]
	public class Set
	{
		public Set()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void Contains()
		{
			//
			// TODO: Add test logic here
			//

			var a=nilnul.num.natural.prime.Set.Contains(44453);

			 Assert.IsTrue(a);

			 var b = nilnul.num.natural.prime.Set.Contains(1);
			 Assert.IsFalse(b);

			 var c = nilnul.num.natural.prime.Set.Contains(0);
			 Assert.IsFalse(c);
			 var d = nilnul.num.natural.prime.Set.Contains(-1);
			 Assert.IsFalse(d);



		}
	}
}
