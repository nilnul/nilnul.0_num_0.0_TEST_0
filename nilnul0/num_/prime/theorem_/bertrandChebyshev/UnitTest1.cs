using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Numerics;

namespace nilnul.num.nilnul0.num_.plural_.prime.theorem_.bertrandChebyshev
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

		}

		nilnul.num.op_.TotientA totient;

		bool predicate_0plural( BigInteger x) {

			return totient.op(x) - totient.op(x / 2) >= 1;

			/// eg:
			///  x =2:
			///		t(2) -t(1) = 1 -0
			///		t(3) -t(1.5) =t(3) -t(1) =2 -0
			///		t(4) -t(2) = 2 -1
			///
			///
			/// 
			///for n > 1
			/// if there is a prime in [n,2n], 
			/// ,then there is a prime in [n, 2n-1]
			///
			///
			/// for 2k
			/// , t(2k) -t(k)>=1
			/// , so there is a prime in [k+1, 2k -1]
			/// 
		}
	}
}
