using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul.bit.str;
using nilnul.obj.op;
using nilnul.obj.str;
using nilnul.obj.str_.future_;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;

namespace nilnul.num.nilnul0.num_.plural_.prime.theorem_.primePairsUnderEven
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			///

			var t=v(10);

			t.Each(
				a=>
				Debug.WriteLine(
					$"{a.Item1} + {a.Item2} = {a.Item1+a.Item2}"
				)
			);
		}

		List<(BigInteger, BigInteger)> v(int evenNum) {
			/*
@JanSchwede
·
It can even be proofed in one tweet:
Take the smallest prime p > 2n
Pair { 2n p-2n}. { 2n - 1,p - 2n + 1},... (all add up to p).
This is always possible, since the next prime is never greater than double of 2n - 1.
This leaves a strictly smaller set of the same form.

			*/


			var nextPrime = nilnul.num_.plural_.prime.set_.db_.ef.AutoGrowMem.SequenceUpTo_inBigInteger__positive(
				evenNum*2
			).First( p=> p>evenNum);

			var pairs = new List<(BigInteger, BigInteger)>();

			for (int i = 0; i < evenNum/2; i++)
			{
				int item0 = evenNum - i;
				pairs.Add(
					(
					  item0
					  ,
					  nextPrime - item0
					)
				);

			}

			return pairs;


		}
	}
}
