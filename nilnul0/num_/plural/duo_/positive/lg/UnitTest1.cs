﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Numerics;

namespace nilnul._num_._TEST_.nilnul0.num_.plural.duo_.positive.lg
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			v(2, 1, 0);
			v(2, 2, 1);
			v(2, 3, 1);
			v(2, 4, 2);
			v(2, 5, 2);
			v(2, 127, 6);
			v(2, 128, 7);
			v(2, 129, 7);
			v(2, 65535, 15);
			v(2, 65536, 16);
			v(2, 65537, 16);

			v(2, 65536*2-1, 16);
			v(2, 65536*2, 17);
			v(2, 65536*2+1, 17);

			v(3, 1, 0);
			v(3, 2, 0);
			v(3, 3, 1);
			v(3, 4, 1);
			v(3, 8, 1);
			v(3, 9, 2);
			v(3, 10, 2);

			v(3, 27 * 3 * 27 -1,6);
			v(3, 27 * 3 * 27 , 7);
			v(3, 27 * 3 * 27 +1, 7);

			v(3, 27 * 3 * 27 * 3 - 1, 7);
			v(3, 27 * 3 * 27 * 3, 8);
			v(3, 27 * 3 * 27 * 3+1, 8);

			v(10, 100000000-1, 7);
			v(10, 100000000, 8);
			v(10, 100000001, 8);

			v(10, 10000_0000_0000_0000ul-1, 15);
			v(10, 10000_0000_0000_0000ul, 16);
			v(10, 10000_0000_0000_0000ul+1, 16);

			v(10, 10000_0000_0000_0000_000ul-1, 18);
			v(10, 10000_0000_0000_0000_000ul, 19);
			v(10, 10000_0000_0000_0000_000ul+1, 19);
			v(10, 10000_0000_0000_0000_000ul+9, 19);
			v(10, 10000_0000_0000_0000_000ul+10000000, 19);

		}

		void v(BigInteger b, BigInteger x, BigInteger expected)
		{
			Assert.IsTrue(
				nilnul.num_.plural.duo_.positive._LgX.Lg(b,x) == expected
			);
		}
	}
}
