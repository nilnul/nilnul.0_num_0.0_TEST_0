using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul.obj.str;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace nilnul.num_.positive.be_.perfect
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{


			(
				(long[])([1,6,28,496,8128,33550336,8389869056,1374_3869_1328 ])
			).Each(
				p=>Debug.WriteLine(
					num_.positive.be_._PerfectX._Be_0positive(p)
				)
			);
			

		}

		IEnumerable<long> _Perfects_0count(long count) {

			
			for (long i = 0; i < count; i++)
			{
				if (be_._PerfectX._Be_0positive(i))
				{
					yield return i;
				}
			}


		}
	}
}
