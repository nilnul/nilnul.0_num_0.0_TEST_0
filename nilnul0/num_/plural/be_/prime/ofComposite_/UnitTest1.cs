﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Numerics;
using nilnul.num_.plural.be;
using System.Diagnostics;

namespace nilnul._num_._TEST_.nilnul0.num_.plural.be_.prime.ofComposite
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var asTxt = nilnul.txt.of_._CharsX.ConcatAsTxt(
				Enumerable.Repeat('1',19)
			);

			var asBigint = BigInteger.Parse(asTxt);

			asBigint = 49943;
			//asBigint *= 2;
			asBigint = 100000;

			var r=nilnul.num_.plural.be_.Prime.Singleton.Be(asBigint);

			Assert.IsTrue(r);
			Debug.WriteLine(r);

		}
	}
}
