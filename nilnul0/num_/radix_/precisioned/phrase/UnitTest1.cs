﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul.num_;
using System;

namespace nilnul._num_._TEST_.nilnul0.num_.radix_.precisioned.phrase
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			v(
				0
				,2
				,
				"0*2^0"
			);
			v(
				0
				,7
				,
				"0*7^0"
			);
			v(
				0
				,10
				,
				"0*10^0"
			);
			v(
				0
				,16
				,
				"0*16^0"
			);
			v(
				1
				,2
				,
				"1*2^0"
			);
			v(
				1
				,7
				,
				"1*7^0"
			);
			v(
				1
				,10
				,
				"1*10^0"
			);
			v(
				1
				,16
				,
				"1*16^0"
			);
			v(
				2
				,2
				,
				"10*2^0"
			);

			v(
				2
				,7
				,
				"2*7^0"
			);
			v(
				2
				,10
				,
				"2*10^0"
			);
			v(
				2
				,16
				,
				"2*16^0"
			);


			v(
				3
				,2
				,
				"11*2^0"
			);
			v(
				3
				,3
				,
				"10*3^0"
			);

			v(
				3
				,10
				,
				"3*10^0"
			);

			v(
				3
				,50
				,
				"3*50^0"
			);


			v(
				4
				,2
				,
				"100*2^0"
			);
			v(
				4
				,3
				,
				"11*3^0"
			);
			v(
				4
				,10
				,
				"4*10^0"
			);
			v(
				4
				,16
				,
				"4*16^0"
			);

			v(
				7
				,16
				,
				"7*16^0"
			);
			v(
				10
				,10
				,
				"10*10^0"
			);

			v(
				15
				,10
				,
				"15*10^0"
			);

			v(
				15
				,16
				,
				"F*16^0"
			);

			v(
				16
				,10
				,
				"16*10^0"
			);
			v(
				16
				,16
				,
				"10*16^0"
			);

			v(
				256+15*16
				,16
				,
				"1F0*16^0"
			);

			v(
				2500+15*50+1
				,50
				,
				"1F1*50^0"
			);
			v(
				2500+15*50+(10+26+1-1)
				,50
				,
				"1Fa*50^0"
			);


		}

		void v(int x, int radic, string expected) {
			v(
				nilnul.Num_ofIn.Of(x)
				,
				radic
				,
				expected
			);
		}

		private void v(Num_ofIn num_ofIn, int radic,string expected)
		{
			v(
				nilnul.num_.radix_.precisioned.of_._OfNumX.Of(num_ofIn, radic)
				,
				expected
			);
		}

		private void v(nilnul.num_.radix_.Precisioned radix2, string expected)
		{
			v(
				nilnul.num_.radix_.precisioned._PhraseX.Phrase(radix2),expected
			);
		}

		void v(string actual,string expected) {
			Assert.IsTrue(actual==expected);
		}
	}
}
