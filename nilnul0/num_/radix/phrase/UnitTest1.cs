﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using nilnul.num_;
using System;

namespace nilnul._num_._TEST_.nilnul0.num_.radix.phrase
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			v(
				0
				,2
				,
				"0"
			);
			v(
				0
				,7
				,
				"0"
			);
			v(
				0
				,10
				,
				"0"
			);
			v(
				0
				,16
				,
				"0"
			);
			v(
				1
				,2
				,
				"1"
			);
			v(
				1
				,7
				,
				"1"
			);
			v(
				1
				,10
				,
				"1"
			);
			v(
				1
				,16
				,
				"1"
			);
			v(
				2
				,2
				,
				"10"
			);

			v(
				2
				,7
				,
				"2"
			);
			v(
				2
				,10
				,
				"2"
			);
			v(
				2
				,16
				,
				"2"
			);


			v(
				3
				,2
				,
				"11"
			);
			v(
				3
				,3
				,
				"10"
			);

			v(
				3
				,10
				,
				"3"
			);

			v(
				3
				,10
				,
				"3"
			);


			v(
				4
				,2
				,
				"100"
			);
			v(
				4
				,3
				,
				"11"
			);
			v(
				4
				,10
				,
				"4"
			);
			v(
				4
				,16
				,
				"4"
			);

			v(
				7
				,16
				,
				"7"
			);
			v(
				10
				,10
				,
				"10"
			);

			v(
				15
				,10
				,
				"15"
			);

			v(
				15
				,16
				,
				"F"
			);

			v(
				16
				,10
				,
				"16"
			);
			v(
				16
				,16
				,
				"10"
			);



		}

		void v(int x, int radic, string expected) {
			v(
				nilnul.Num_ofIn.Of(x)
				,
				radic
				,
				expected
			);
		}

		private void v(Num_ofIn num_ofIn, int radic,string expected)
		{
			v(
				nilnul.num_.radix.of_._OfNumX._Of_1nonneg(radic,num_ofIn)
				,
				expected
			);
		}

		private void v(Radix2 radix2, string expected)
		{
			v(
				nilnul.num_.radix._PhraseX.Phrase(radix2),expected
			);
		}

		void v(string actual,string expected) {
			Assert.IsTrue(actual==expected);
		}
	}
}
