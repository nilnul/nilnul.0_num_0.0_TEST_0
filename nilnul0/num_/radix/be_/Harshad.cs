﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.num_.radix.be_
{
	/// <summary>
	/// 
	/// </summary>
	internal class Harshad
		:
		nilnul.obj.BeA_ofIn<
			nilnul.num_.Radix2
		>
	{
		public override bool be(in Radix2 val)
		{
			var sum = val.digits.Aggregate(BigInteger.Zero,(a,c)=>a+c);
			if (sum ==0)
			{
				return true;
			}

			return val.toBigint() % sum == 0;

		}


		static public Harshad Unison
		{
			get
			{
				return nilnul._obj.typ_._UnisonX<Harshad>.Unison;
			}
		}

	}
}
