using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.num_.prime_.eg_
{
	/// <summary>
	/// discovered by Harvey Dubner
	/// </summary>
	internal class _HarveyDubnerX
	{ 
		static public BigInteger Calculate() {

			return BigInteger.Pow(10,6400) - 1	/// all 9s
				-BigInteger.Pow(10, 6352)		/// one 9 would become 8
				;
		}
	}
}
