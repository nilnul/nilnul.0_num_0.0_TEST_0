﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace nilnul._num_._TEST_.op_.unary_.factorial_.memoize
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			Parallel.For(
				0, 60,

				i =>

				vow(
					i
					,
					nilnul.num.op_.unary_.factorial_.Memoize.Singleton._assumeNat(i)
				)
			);

			//Here, we were calling memoized(0) a 1000 times(with Parallel.For), but our counter says 9.Why is that ? The GetOrAdd on Dictionary is called after initial function has returned a result, so it just happens that each thread calls it’s own function as none are present in the dictionary initially.
		}


		Dictionary<BigInteger, BigInteger> dict = new Dictionary<BigInteger, BigInteger>()
		{
			[0] = 1
				,
			[1] = 1
				,
			[2] = 2
				,
			[3] = 6
				,
			[4] = 24
				,
			[9] = 362880
				,
			[10] = 3628800
		};
		void vow(BigInteger x, BigInteger y)
		{

			if (dict.ContainsKey(x))
			{
				Debug.Assert(
					y == dict[x]
				);
			}

		}


	}
}
