﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul.op_.unary_.bitly_.left_
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var a = nilnul.num.op_.unary_.bitly_.shift_._LeftX.ShiftLeft(15);
			Assert.IsTrue(
				a== ((int)  short.MaxValue)			//2^15-1
				+1
			);

			int shortMax = short.MaxValue;
			shortMax++;
			shortMax *= shortMax;	// 2^30
			

			var r = nilnul.num.op_.unary_.bitly_.shift_._LeftX.ShiftLeft(30);

			Assert.IsTrue(
				r==shortMax
			);

			try
			{
				var t= nilnul.num.op_.unary_.bitly_.shift_._LeftX.ShiftLeft(31);
				throw new UnexpectedReachException();
			}
			catch (OverflowException )
			{

				
			}
		}
	}
}
