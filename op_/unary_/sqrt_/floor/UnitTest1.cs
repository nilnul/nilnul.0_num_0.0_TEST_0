﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace nilnul.op_.unary_.sqrt_.floor
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var intMax = int.MaxValue; //2^31-1

			var expected = (1l << 31);// - 1;
			expected--;
			Assert.IsTrue(
				intMax ==expected
			);


			var shortMax = short.MaxValue;
			Assert.IsTrue(
				shortMax == (1<<15)-1
			);

			var shortMaxSquare =(int) shortMax * (int)shortMax;  // lt 2^30




			var intMaxSq =(long) intMax * (long) intMax;

			Assert.IsTrue(
				shortMaxSquare <intMax
			);

			/// find sqrt of intMax;
			///
			/// 1<<31
			///  (1<<15, 1<<16)

			var intMaxSqrt = nilnul.num.op_.unary_.sqrt_._FloorX.SqrtFloorOfIntMax;

			var squaredBack = intMaxSqrt * intMaxSqrt;

			Assert.IsTrue(squaredBack<=int.MaxValue);
			Assert.IsTrue(squaredBack<int.MaxValue);


			long intMaxSqrtPlus = intMaxSqrt + 1;

			Assert.IsTrue(intMaxSqrtPlus*intMaxSqrtPlus>int.MaxValue);




			for (int i = 0; i < 10000; i++)
			{
				v(i);
			}


			for (int i = int.MaxValue; i >int.MaxValue- 10000; i--)
			{
				v(i);
			}




		}

		void v(int x) {
			var s = nilnul.num.op_.unary_.sqrt_._FloorX._SqrtFloor_0nonneg(x);

			Assert.IsTrue(s*s<=x);

			long sPlus = s + 1;
			Assert.IsTrue(sPlus*sPlus>x);

		}
	}
}
